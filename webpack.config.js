//usamos los plugin para poder comprimir css y html
const HtmlWebpackPlugin = require('html-webpack-plugin');

//me permite ingresar un objeto js:
module.exports = {
    entry:'./src/app.js',
    output:{
        //le damos una ruta absoluta por eso usamos _dirname
        path: __dirname +'/build',
        filename:'bundle.js'
        //bundle.js es lo mismo que output.js
    },
    /*
    //podemos configurar el puerto de desarrollo
      devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  }
    
    */ 
   //usamos modules para decirles que archivos procesar:
   //puedes testear todos los archivos que terminen en css o sass.
   module:{
       rules:[
           {
                test:/\.scss$/,
                use:[
                    {loader:"style-loader"},
                    {loader:"css-loader",},
                    {loader:"sass-loader",}

                ]
           }
       ]
   },
    //usamos plugins
    plugins:[
        new HtmlWebpackPlugin({
            //le pasamos el codigo de origen:
            template:'./src/index.html',
        })
    ]

    
}